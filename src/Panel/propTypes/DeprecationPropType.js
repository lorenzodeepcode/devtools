import PropTypes from 'prop-types';

const DeprecationPropType = PropTypes.shape({
  timestamp: PropTypes.number.isRequired,
  node: PropTypes.node.isRequired,
  nodeId: PropTypes.string.isRequired,
  selector: PropTypes.node.isRequired,
  displayName: PropTypes.node.isRequired,
  auiVersion: PropTypes.node.isRequired,
  version: PropTypes.node.isRequired,
  alternativeName: PropTypes.node,
  removeVersion: PropTypes.node,
  extraInfo: PropTypes.node
});

export default DeprecationPropType;
