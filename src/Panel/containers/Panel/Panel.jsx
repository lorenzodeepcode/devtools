import React from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import PanelWrapper from '../../components/PanelWrapper/PanelWrapper';
import PanelContent from '../../components/PanelContent/PanelContent';
import PanelToolbar from '../PanelToolbar/PanelToolbar';

import DeprecationsList from '../DeprecationsList/DeprecationsList';

import auiDetector from '../../services/aui/auiDetector';
import getVersion from '../../services/aui/getVersion';
import { hasAui, setVersion, wasLoaded } from '../../actions/panel';

const withAui = lifecycle({
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(wasLoaded());

    Promise.all([
      auiDetector(),
      getVersion()
    ]).then(([hasAuiResult, auiVersion]) => {
      dispatch(hasAui(hasAuiResult));
      dispatch(setVersion(auiVersion));
    });
  }
});

const Panel = () => (
  <PanelWrapper>
    <PanelToolbar />
    <PanelContent>
      <DeprecationsList />
    </PanelContent>
  </PanelWrapper>
);

export default compose(
  connect(),
  withAui
)(Panel);
