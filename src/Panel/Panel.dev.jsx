import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import store from './store';
import PanelApp from './containers/PanelApp/PanelApp';

const renderApp = (Component, rootId = 'root') => {
  const targetEl = document.getElementById(rootId);

  render(
    (
      <AppContainer warnings={false}>
        <Component store={store} />
      </AppContainer>
    ), targetEl
  );
};

renderApp(PanelApp);

if (module.hot) {
  module.hot.accept('./Panel/Panel', () => render(PanelApp));
}
