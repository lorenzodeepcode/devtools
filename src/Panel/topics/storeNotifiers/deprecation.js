import { DEPRECATION } from '../contants';
import { storeDeprecation } from '../../actions/deprecations';

const notifyAboutDeprecation = (attachNotifier, store) => {
  console.debug('P: Notifier: Registering "notifyAboutDeprecation" notifier');

  attachNotifier(DEPRECATION, (deprecation) => {
    console.debug('P: Notifier: Dispatching "storeDeprecation" action', deprecation);

    store.dispatch(storeDeprecation(deprecation));
  });
};

export default notifyAboutDeprecation;
