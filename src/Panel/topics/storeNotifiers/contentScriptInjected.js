import { CONTENT_SCRIPT_INJECTED } from '../contants';
import { toggleHighlightNodes } from '../messages/highlithing';

const contentScriptInjected = (attachNotifier, store) => {
  console.debug('P: Notifier: Registering "contentScriptInjected" notifier');

  attachNotifier(CONTENT_SCRIPT_INJECTED, ({ deprecation }) => {
    console.debug('P: Notifier: Dispatching "contentScriptInjected" action', deprecation);

    const { highlighting } = store.getState();
    const { highlightDeprecatedElements } = highlighting;

    if (highlightDeprecatedElements) {
      toggleHighlightNodes(highlightDeprecatedElements);
    }
  });
};

export default contentScriptInjected;
