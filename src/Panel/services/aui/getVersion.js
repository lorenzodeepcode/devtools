import evaluateCodeInInspectedWindow from '../chrome/evaluteInInspectedWindow';

const versionStatement = 'window.AJS && window.AJS.version';

const getVersion = () => evaluateCodeInInspectedWindow(versionStatement);

export default getVersion;
