const listeners = [];

// TODO: Extract Chrome API for mocking and testing purpose
const portConnection = chrome.runtime.connect({
  name: 'asd_content_script'
});

export const emit = (topic, message) => {
  portConnection.postMessage({ ...message, topic });
};

export const on = (topic, handler) => {
  listeners.push({ topic, handler });
};

export const initConnection = () => {
  emit('INIT_CONTENT_SCRIPT');

  portConnection.onMessage.addListener(({ topic, ...message }) => {
    console.debug(`CS: Received message ${topic}`, message);

    listeners.forEach(({ topic: listenerTopic, handler }) => {
      if (topic === listenerTopic) {
        handler(message);
      }
    });
  });
};
