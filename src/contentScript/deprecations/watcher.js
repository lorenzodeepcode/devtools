import MutationObserver from './mutationObserver';
import elementMatchesSelector from './matcher';

const watching = [];

export const REMOVED_NODE = Symbol('REMOVED_NODE');
export const ADDED_NODE = Symbol('ADDED_NODE');

const wrapNotification = (selector, node, deprecationConfig) => ({
  ...deprecationConfig,
  selector,
  node
});

const isSupportedNode = node => (node instanceof Element);

const iterateAddedNode = (node, notifier) => {
  if (!isSupportedNode(node)) {
    return;
  }

  watching.forEach(({ selector, ...deprecationConfig }) => {
    if (elementMatchesSelector(node, selector)) {
      notifier(ADDED_NODE, wrapNotification(selector, node, deprecationConfig));
    }
  });
};

const iterateRemovedNode = (node, notifier) => {
  if (!isSupportedNode(node)) {
    return;
  }

  notifier(REMOVED_NODE, { node });
};

const iterateMutations = ({ addedNodes, removedNodes }, notifier) => {
  Array.from(addedNodes).forEach(node => iterateAddedNode(node, notifier));
  Array.from(removedNodes).forEach(node => iterateRemovedNode(node, notifier));
};

const mutationsHandler = (mutations, notifier) => (
  mutations.forEach(mutationChange => iterateMutations(mutationChange, notifier))
);

const initObserver = (target, notifier) => {
  const observer = new MutationObserver(mutations => mutationsHandler(mutations, notifier));

  const config = {
    childList: true,
    subtree: true
  };

  observer.observe(target, config);

  return observer;
};

const scanDom = (target, notifier) => {
  console.debug('CS: Scanning current DOM state');

  watching.forEach(({ selector, ...deprecationConfig }) => {
    try {
      const result = target.querySelectorAll(selector);

      Array.from(result).forEach(node => (
        notifier(ADDED_NODE, wrapNotification(selector, node, deprecationConfig))
      ));
    } catch (e) {
      console.debug(`CS: Wrong or empty selector "${selector}`, e);
    }
  });
};

const watchSelector = ({
  selector, displayName, alternativeName, extraInfo, auiVersion, version, removeVersion
}) => {
  watching.push({
    selector,
    displayName,
    alternativeName,
    extraInfo,
    auiVersion,
    version,
    removeVersion
  });
};

export const initWatcher = (notifier, target = window.document) => {
  const attachListener = () => {
    if (document.readyState !== 'complete') {
      return;
    }

    scanDom(target, notifier);
    initObserver(target, notifier);
  };

  if (document.readyState === 'complete') {
    attachListener();
  } else {
    document.addEventListener('readystatechange', attachListener);
  }
};

export const registerSelector = ({ selector, ...config }) => {
  if (Array.isArray(selector)) {
    selector.forEach(cssSelector => watchSelector({
      ...config,
      selector: cssSelector
    }));
  } else {
    watchSelector({ selector, ...config });
  }
};
