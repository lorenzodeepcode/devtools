import store from '../store';
import { toggleHighlightAllNodes } from '../actions/deprecationHighlighter';

export default attachListener => (
  attachListener('DEVTOOLS_CLOSED', () => {
    store.highlightNodes = false;

    toggleHighlightAllNodes();
  })
);
