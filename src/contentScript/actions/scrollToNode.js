import { getNode } from '../deprecations/registry';

const scrollToNode = (nodeId) => {
  const node = getNode(nodeId);

  if (!node) {
    return;
  }

  // eslint-disable-next-line no-unused-expressions
  (typeof node.scrollIntoViewIfNeeded === 'function') ? node.scrollIntoViewIfNeeded() : node.scrollIntoView();
};

export default scrollToNode;
